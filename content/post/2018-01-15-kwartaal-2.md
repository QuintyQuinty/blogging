---   
Title: Logboek   
Subtitle: Blog kwartaal 2 Januari   
Date: 2018-01-15   
Tags: ["Blog"]   
---   

**Ma 8 januari**   
Studiecoaching gekregen  
Design rationale workshop gevolgd  
Verder aan het design rationale werken 

**Di 9 januari**    
Hoorcollege visuele beinvloeding gevolgd    
Design rationale afgemaakt met Rosa  
Workshop testplan gevolgd   
thuis verder gaan leren voor tentamen    

**Wo 10 januari**   
Validatie moment van Design rationale   
Doordat het niet was goedgekeurd hebben rosa en ik nog een nieuw concept bedacht met behulp van Robin.    

**Do 11 januari**   
Tentamen gemaakt   

**Za 13 januari**   
Aan het leerdossier werken, verder met starrs schrijven.   

**Ma 15 januari**   
Testplan high-fid prototype en concept poster gemaakt gemaakt met Rosa   
Verder aan leerdossier.    

**Di 16 Januari**  
Verder met leerdossier 
Naar validatie testplan high-fid prototype gegaan (voldoende gevalideerd).
Indesign gevolgd.

**Wo 17 Januari**
Eerder naar school gegaan om conceptposter nog af te maken en details voor de expo af te maken.   
Expo tafel opgezet en gaan eten. Half 7 begon de expo.   
De opdrachtgever kwam niet langs onze tafel en de prijs uitrijking is niet geweest.   
Feedback gekregen van anderen. 

**Do 18 Januari**   
Infographics gevolgd.   
Leerdossier afgemaakt en ingeleverd.   







