---
Title: Logboek
Subtitle: Blog iteratie 3
Date: 2017-10-25
Tags: ["blog"]
---

**9 okt (ma)**   
Nieuwe iteratie, veranderingen aan het spel maken: rekening houden met de feedback van woensdag.   
**10 okt (di)**    
Geen hoorcollege, leren voor tentamen design theory. Minigames verzinnen.   
Details verzinnen voor in de escaperoom.   
**11 okt (wo)**  
Verder aan spel. Workshop leesdossier. Verder aan de minigames met Serhat en Puck.   
**12 okt (do)**   
Illustrator gevolgd. Tentamen gemaakt.   
**16 okt (ma)**  
Met team afgesproken om verder aan het spel te werken.   
**22 okt (zo)**  
Blog bijgewerkt, minigames uitgewerkt, details voor (vernieuwde) escaperoom uitgewerkt.   
**23 okt (ma)**  
Nog meer details voor de escaperoom maken, door voor de escaperoom verven.    
Ontwerpproceskaart gemaakt. Overzicht gemaakt van wat nog af moet  niet alles af gekregen, morgen verder.     
**24 okt (di)**   
Minigames getest, escaperoom afgemaakt, logo + naam bedacht (en ontworpen), spelanalyse gemaakt, spelregels nagekeken en aangekruist op de map waar de minigames zich bevinden.    
Oneliner voor expo bedacht.   
**25 okt (wo)**   
Expositie gehad. Feedback gekregen. Peerfeedback ingevuld en zelf teruggekregen.    
Studie coaching gehad over het leerdossier.   
Blog bijwerken, stars schrijven.   
