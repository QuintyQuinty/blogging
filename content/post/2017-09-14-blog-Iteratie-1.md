---
Title: Logboek
Subtitle: Blog iteratie 1
Date: 2017-09-14
tags: ["blog"]
---
**30 aug (wo)**       
Onze groepsnaam, -logo en -icoon bedacht.    
 **31 aug (do)**    
Hoorcollege gekregen en onze opdracht ontvangen.    
**4 september (ma)**    
We hebben een team captain uitgekozen --> Puck   
Samen met het team de regels vaststellen, teamposters gemaakt, taakverdeling/planning gemaakt.   
studiecoaching gekregen   
**5 sept (di)**    
Hoorcollege Design theorie   
Verder met onderzoeken en spullen voor woensdag voor het moodboard verzamelen. (alle verzamelde afbeeldingen zijn zwart-wit uitgeprint dus het moest digitaal).   
**6 sept (wo)**    
Moodboard digitaal afgemaakt.   
Workshop blog maken   
**7 sept (do)**    
Werk college design challange gehad   
**8 sept (vr)**    
Aantekeningen digitaal overgenomen   
Nog onderzoek/onderzoeksvragen/moodboards erin zetten   
**11 sept (ma)**    
Met het team één spel van de vijf individuele bedachte spellen uitgekozen.   
We hebben nog van de overige spellen bepaalde stukjes erbij gevoegd.   
Taken verdeeld   
**12 sept (di)**    
Hoorcollege design theory: verbeelden   
**13 sept (wo)**    
Workshop moodboard gehad   
Verder aan ons spel   
Studie coaching gehad   
**14 sept (do)**     
Keuzevak illustrator gevolgd    
Feedback gevraagd --> feedback gekregen   
Spelanalyse gemaakt   
