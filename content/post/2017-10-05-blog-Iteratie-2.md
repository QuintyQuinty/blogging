---
Title: Logboek
Subtitle: Blog iteratie 2
Date: 2017-10-05
Tags: ["blog"]
---

**18 sept (vr)**    
Paperprototype presenteren   
Briefing 2 ontvangen --> kill your darling   
Nieuwe planning maken   
Inschrijven studiekeuzevak   
**19 sept (di)**  
Hoorcollege prototyping   
**20 sept (wo)**  
Spel analyse van 3 bestaande spellen; tafelvoetbal, saboteurs, fifa    
Onderzoeken van spelregels   
**21 sept (do)**   
Illustrator gevolgd   
Werkcollege gekregen   
**25 sept (ma)**  
3 concepten met elkaar bedacht: escaperoom, geochache, lasergamen   
Feedback gekregen van Bob, geprobeerd om de 3 concepten samen te brengen.   
**26 sept (di)**  
Hoorcollege onderzoeken gehad   
**27 sept (wo)**  
Verder met het uitwerken van ons project; beginnen aan paperprototype escaperoom   
**28 sept (do)**  
Illustrator gehad, verder met paperprototype maken: escaperoom afgemaakt, verder aan map van Rotterdam en schermen voor het online gedeelte van het spel.   
Werkcollege gehad.   
**29 sept (vr)**   
0-meting handvaardigheid.   
**1 okt (zo)**   
Met team besproken over hoe het gaat met het teamwerk/inzet, naar aanleiding van Puck haar mail.   
**2 okt (ma)**   
Laatste dingen van het prototype afmaken   
Presentatie stukken verdelen: ik vertel het spelverloop, dus tekst over spelverloop maken.   
**3 okt (di)**   
Geen hoorcollege, tekst van presentatie doornemen.   
**4 okt (wo)**   
Presentatie dag, voor alumni presenteren. Feedback gekregen.   
**5 okt (do)**  
Illustrator gehad   
0 meting Nederlands en Engels   
